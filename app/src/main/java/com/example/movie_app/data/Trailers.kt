package com.example.movie_app.data

data class Trailers(val id: Long, val results: List<TrailerList>)
