package com.example.movie_app.data

data class Movie(val total_results: Int, val results: List<MoviesList>)
