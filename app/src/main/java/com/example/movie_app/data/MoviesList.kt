package com.example.movie_app.data

data class MoviesList(val poster_path: String, val title: String, val release_date: String, val vote_average: Double, val overview: String, val id: Long)
