package com.example.movie_app

import com.example.movie_app.data.Movie
import com.example.movie_app.data.Reviews
import com.example.movie_app.data.Trailers
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

const val BASE_URL = "https://api.themoviedb.org/"

interface MoviesOfInterface {

    @GET("https://api.themoviedb.org/3/movie/popular?api_key=65db5aebb7dc29d77c7b00443904e829")
    fun getMovies(): Call<Movie>

    @GET("https://api.themoviedb.org/3/movie/{movieId}/videos?api_key=65db5aebb7dc29d77c7b00443904e829")
    fun getTrailers(@Path("movieId") id: Long): Call<Trailers>


    @GET("https://api.themoviedb.org/3/movie/{movieId}/reviews?api_key=65db5aebb7dc29d77c7b00443904e829")
    fun getReviews(@Path("movieId") id: Long): Call<Reviews>

}

object MovieInterface {
    val movieInstance: MoviesOfInterface

    init {
        val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        movieInstance = retrofit.create(MoviesOfInterface::class.java)
    }
}
