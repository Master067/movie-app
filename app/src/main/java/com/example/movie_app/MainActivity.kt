package com.example.movie_app

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.movie_app.ui.main.movie.MainFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, MainFragment.newInstance())
                .commit()

    }
}
