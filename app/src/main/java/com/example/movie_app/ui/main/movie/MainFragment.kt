package com.example.movie_app.ui.main.movie

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.movie_app.R
import com.example.movie_app.adapter.MovieAdapter
import com.example.movie_app.data.MoviesList
import com.example.movie_app.ui.main.masterDetail.MasterDetailActivity

class MainFragment : Fragment(), MovieAdapter.OnMovieClickListener {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.main_fragment, container, false)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        viewModel.getMovies()
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recyclerView = view.findViewById<RecyclerView>(R.id.movie_list)
        viewModel.result.observe(viewLifecycleOwner) {
            val movieAdapter = MovieAdapter(it, this)
            recyclerView.layoutManager = GridLayoutManager(requireActivity(), 2)
            recyclerView.adapter = movieAdapter
        }
    }

    override fun onMovieClick(item: MoviesList, position: Int) {
        //  https://api.themoviedb.org/3/movie/{movie_id}/reviews?api_key={sidhfd}
        //  https://api.themoviedb.org/3/movie/{movie_id}/videos?api_key={sidhfd}
        val intent = Intent(context, MasterDetailActivity::class.java)
        intent.putExtra("poster_path", item.poster_path)
        intent.putExtra("title", item.title)
        intent.putExtra("release_date", item.release_date)
        intent.putExtra("vote_average", item.vote_average.toString())
        intent.putExtra("overview", item.overview)
        intent.putExtra("id", item.id)
        startActivity(intent)
    }
}
