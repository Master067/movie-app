package com.example.movie_app.ui.main.masterDetail

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.movie_app.MovieInterface
import com.example.movie_app.data.ReviewList
import com.example.movie_app.data.Reviews
import com.example.movie_app.data.TrailerList
import com.example.movie_app.data.Trailers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MasterDetailViewModel : ViewModel() {
    private val _result = MutableLiveData<List<TrailerList>>()
    val result: LiveData<List<TrailerList>> = _result

    private val _reviewResult = MutableLiveData<List<ReviewList>>()
    val reviewResult: LiveData<List<ReviewList>> = _reviewResult

    fun getTrailers(id: Long) = viewModelScope.launch {
        withContext(Dispatchers.IO) {
            val movies: Call<Trailers> = MovieInterface.movieInstance.getTrailers(id)
            movies.enqueue(object : Callback<Trailers> {

                override fun onResponse(call: Call<Trailers>, response: Response<Trailers>) {
                    val movies: Trailers? = response.body()
                    if (movies != null) {
                        _result.postValue(movies.results)
                    }
                }

                override fun onFailure(call: Call<Trailers>, t: Throwable) {
                    Log.d("SUPERCODE", "Error in fetching Movies", t)
                }
            })
        }
    }

    fun getReviews(id: Long) = viewModelScope.launch {
        withContext(Dispatchers.IO) {
            val movies: Call<Reviews> = MovieInterface.movieInstance.getReviews(id)
            movies.enqueue(object : Callback<Reviews> {
                override fun onResponse(call: Call<Reviews>, response: Response<Reviews>) {
                    val movies: Reviews? = response.body()
                    if (movies != null) {
                        _reviewResult.postValue(movies.results)
                    }
                }

                override fun onFailure(call: Call<Reviews>, t: Throwable) {
                    Log.d("SUPERCODE", "Error in fetching Reviews", t)
                }


            })
        }
    }
}
