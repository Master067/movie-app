package com.example.movie_app.ui.main.movie

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.movie_app.MovieInterface
import com.example.movie_app.data.Movie
import com.example.movie_app.data.MoviesList
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel : ViewModel() {

    private val _result = MutableLiveData<List<MoviesList>>()
    val result: LiveData<List<MoviesList>> = _result

    fun getMovies() = viewModelScope.launch {
        withContext(Dispatchers.IO) {
            val movies: Call<Movie> = MovieInterface.movieInstance.getMovies()
            movies.enqueue(object : Callback<Movie> {
                override fun onResponse(call: Call<Movie>, response: Response<Movie>) {
                    val movies: Movie? = response.body()
                    if (movies != null) {
                        _result.postValue(movies.results)
                    }
                }

                override fun onFailure(call: Call<Movie>, t: Throwable) {
                    Log.d("SUPERCODE", "Error in fetching Movies", t)
                }
            })
        }
    }
}
