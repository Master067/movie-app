package com.example.movie_app.ui.main.masterDetail

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.movie_app.R
import com.example.movie_app.adapter.ReviewAdapter
import com.example.movie_app.adapter.TrailerAdapter
import com.example.movie_app.data.TrailerList
import com.google.android.material.appbar.CollapsingToolbarLayout

class MasterDetailActivity : AppCompatActivity(), TrailerAdapter.OnButtonClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_master_detail)
        val poster = findViewById<ImageView>(R.id.toolbar_image)
        val titleText = findViewById<TextView>(R.id.title)
        val releaseDate = findViewById<TextView>(R.id.release_date)
        val rateBar = findViewById<RatingBar>(R.id.rate_bar)
        val overview = findViewById<TextView>(R.id.overview)
        val collapse = findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout)
        val toolbar = findViewById<Toolbar>(R.id.detail_toolbar)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview_trailer_container)
        val recyclerViewReview = findViewById<RecyclerView>(R.id.recyclerview_review_container)

        val intent = intent
        titleText.text = intent.getStringExtra("title")
        releaseDate.text = intent.getStringExtra("release_date")
        val voteAverage = intent.getStringExtra("vote_average")
        val id = intent.getLongExtra("id", 0)
        val rate = voteAverage?.toFloat()
        rateBar.rating = (rate ?: 0.0F) / 2.0F
        val posterPath = intent.getStringExtra("poster_path")
        val imagePath = "https://image.tmdb.org/t/p/w500${posterPath}"
        Glide.with(this).load(imagePath).into(poster)
        val overviewString = intent.getStringExtra("overview")
        collapse.title = titleText.text
        overviewString?.let {
            addReadMore(it, overview)
        }
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val viewModel: MasterDetailViewModel = ViewModelProvider(this).get(MasterDetailViewModel::class.java)
        viewModel.getTrailers(id)
        viewModel.getReviews(id)

        val trailerAdapter = TrailerAdapter(this)
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.adapter = trailerAdapter

        val reviewAdapter = ReviewAdapter()
        recyclerViewReview.layoutManager=LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)
        recyclerViewReview.adapter = reviewAdapter

        viewModel.result.observe(this) {
            if (!it.isNullOrEmpty()) {
                trailerAdapter.addTrailers(it)
            }
        }

        viewModel.reviewResult.observe(this) {
            if (!it.isNullOrEmpty()) {
                reviewAdapter.adReviews(it)
            }
        }


    }

    fun addReadMore(text: String, textView: TextView) {
        val ss = SpannableString(text.substring(0, 100) + "... show more")
        val clickableSpan: ClickableSpan = object : ClickableSpan() {
            override fun onClick(view: View) {
                addReadLess(text, textView)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.setUnderlineText(false)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ds.setColor(getResources().getColor(R.color.teal_200, getTheme()))
                } else {
                    ds.setColor(getResources().getColor(R.color.teal_200))
                }
            }
        }
        ss.setSpan(clickableSpan, ss.length - 10, ss.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        textView.setText(ss)
        textView.setMovementMethod(LinkMovementMethod.getInstance())
    }

    fun addReadLess(text: String, textView: TextView) {
        val ss = SpannableString("$text show less")
        val clickableSpan: ClickableSpan = object : ClickableSpan() {
            override fun onClick(view: View) {
                addReadMore(text, textView)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.setUnderlineText(false)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ds.setColor(getResources().getColor(R.color.teal_200, getTheme()))
                } else {
                    ds.setColor(getResources().getColor(R.color.teal_200))
                }
            }
        }
        ss.setSpan(clickableSpan, ss.length - 10, ss.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        textView.setText(ss)
        textView.setMovementMethod(LinkMovementMethod.getInstance())
    }

    override fun onButtonClick(item: TrailerList, position: Int) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=${item.key}"))
        startActivity(intent)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
}
