package com.example.movie_app.viewHolder

import android.os.Build
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.movie_app.R
import com.example.movie_app.data.ReviewList
import com.example.movie_app.ui.main.masterDetail.MasterDetailActivity

class ReviewViewHolder(view: View) : RecyclerView.ViewHolder(view)  {
    var review_text = itemView.findViewById<TextView>(R.id.review_item)

    fun intialize(item: ReviewList) {
        review_text.text = item.content
        review_text?.let {
            addReadMore(it.text.toString(), review_text)
        }
    }
    fun addReadMore(text: String, textView: TextView) {
        val ss = SpannableString(text.substring(0, 100) + "... show more")
        val clickableSpan: ClickableSpan = object : ClickableSpan() {
            override fun onClick(view: View) {
                addReadLess(text, textView)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.setUnderlineText(false)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ds.setColor(itemView.context.getResources().getColor(R.color.teal_200, itemView.context.getTheme()))
                } else {
                    ds.setColor(itemView.context.getResources().getColor(R.color.teal_200))
                }
            }
        }
        ss.setSpan(clickableSpan, ss.length - 10, ss.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        textView.setText(ss)
        textView.setMovementMethod(LinkMovementMethod.getInstance())
    }

    fun addReadLess(text: String, textView: TextView) {
        val ss = SpannableString("$text show less")
        val clickableSpan: ClickableSpan = object : ClickableSpan() {
            override fun onClick(view: View) {
                addReadMore(text, textView)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.setUnderlineText(false)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ds.setColor(itemView.context.getResources().getColor(R.color.teal_200, itemView.context.getTheme()))
                } else {
                    ds.setColor(itemView.context.getResources().getColor(R.color.teal_200))
                }
            }
        }
        ss.setSpan(clickableSpan, ss.length - 10, ss.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        textView.setText(ss)
        textView.setMovementMethod(LinkMovementMethod.getInstance())
    }
}