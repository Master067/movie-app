package com.example.movie_app.viewHolder

import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.movie_app.R
import com.example.movie_app.adapter.TrailerAdapter
import com.example.movie_app.data.TrailerList

class TrailerViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    var imageView1 = itemView.findViewById<ImageView>(R.id.trailer_image)
    val playButton = itemView.findViewById<ImageView>(R.id.play_button)
    fun intialize(item: TrailerList, action: TrailerAdapter.OnButtonClickListener) {
        val posterPath = "https://img.youtube.com/vi/${item.key}/0.jpg"
        Glide.with(itemView.context).load(posterPath).into(imageView1)
        playButton.setOnClickListener {
            action.onButtonClick(item, adapterPosition)
        }
    }
}
