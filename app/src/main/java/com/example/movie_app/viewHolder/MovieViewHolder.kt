package com.example.movie_app.viewHolder

import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.movie_app.R
import com.example.movie_app.adapter.MovieAdapter
import com.example.movie_app.data.MoviesList

class MovieViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    var imageView = itemView.findViewById<ImageView>(R.id.image)
    fun intialize(item: MoviesList, action: MovieAdapter.OnMovieClickListener) {
        val posterPath = "https://image.tmdb.org/t/p/w500${item.poster_path}"
        Glide.with(itemView.context).load(posterPath).into(imageView)
        itemView.setOnClickListener {
            action.onMovieClick(item, adapterPosition)
        }
    }
}
