package com.example.movie_app.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.movie_app.R
import com.example.movie_app.data.TrailerList
import com.example.movie_app.viewHolder.TrailerViewHolder

class TrailerAdapter(var clickListener: OnButtonClickListener) : RecyclerView.Adapter<TrailerViewHolder>() {
   private var arrayList = mutableListOf<TrailerList>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrailerViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.trailer_item, parent, false)
        return TrailerViewHolder(view)
    }

    override fun onBindViewHolder(holder: TrailerViewHolder, position: Int) {
        val item: TrailerList = arrayList[position]
        holder.intialize(item, clickListener)
    }

    override fun getItemCount() = arrayList.size

    fun addTrailers(list: List<TrailerList>){
        arrayList.addAll(list)
        notifyDataSetChanged()
    }

    interface OnButtonClickListener {
        fun onButtonClick(item: TrailerList, position: Int)
    }
}
