package com.example.movie_app.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.movie_app.R
import com.example.movie_app.data.MoviesList
import com.example.movie_app.viewHolder.MovieViewHolder

class MovieAdapter(val arrayList: List<MoviesList>, var clickListener: OnMovieClickListener) : RecyclerView.Adapter<MovieViewHolder>() {

    override fun getItemCount() = arrayList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.row, parent, false)
        return MovieViewHolder(view)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val item: MoviesList = arrayList[position]
        holder.intialize(item, clickListener)
    }

    interface OnMovieClickListener {
        fun onMovieClick(item: MoviesList, position: Int)
    }
}
