package com.example.movie_app.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.movie_app.R
import com.example.movie_app.data.ReviewList
import com.example.movie_app.viewHolder.ReviewViewHolder

class ReviewAdapter() : RecyclerView.Adapter<ReviewViewHolder>() {
    private var arrayList = mutableListOf<ReviewList>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.review_item, parent, false)
        return ReviewViewHolder(view)
    }

    override fun onBindViewHolder(holder: ReviewViewHolder, position: Int) {
        val item: ReviewList = arrayList[position]
        holder.intialize(item)
    }
    override fun getItemCount() = arrayList.size

    fun adReviews(list: List<ReviewList>){
        arrayList.addAll(list)
        notifyDataSetChanged()
    }
}
